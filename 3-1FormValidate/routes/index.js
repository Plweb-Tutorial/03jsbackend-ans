var express = require("express");
var router = express.Router();

router.get("/", function(req, res){
    res.render("index");
});
  
router.get("/page1", function(req, res){
    res.render("page1");
});

router.get("/page2", function(req, res){
    res.render("page2");
});

module.exports = router;